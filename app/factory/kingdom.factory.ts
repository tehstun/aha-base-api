import Kingdom from '../models/kingdom.model';

export default class KingdomFactory {
  public static default(): Kingdom {
    return new Kingdom();
  }
}

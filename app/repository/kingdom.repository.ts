import * as _ from 'lodash';
import { EntityRepository, Repository } from 'typeorm';

import Kingdom from '../models/kingdom.model';
import User from '../models/user.model';

@EntityRepository(Kingdom)
export default class KingdomRepository extends Repository<Kingdom> {
  /**
   * Return a array of all the given kingdoms currently in the database.
   */
  public async getAllKingdoms(): Promise<Kingdom[]> {
    return Kingdom.find();
  }

  /**
   * Checks to see if a given kingdom already exists by a given identifer.
   * @param identifer the identifer of the kingdom being searched for.
   */
  public async existsByIdentifer(identifer: string): Promise<boolean> {
    return (await Kingdom.count({ where: [{ name: identifer }] })) >= 1;
  }

  /**
   * Attempts to find a kingdom by a given identifer in the database, if the kingdom does not exist then
   * the return promise value would just be null.
   * @param identifer the identifer of the kingdom being found.
   */
  public async findByIdentifier(identifer: string): Promise<Kingdom> {
    if (_.isNil(identifer)) return null;

    // since finding by a name is more commonly used than by the id, first we will check that the
    // identifer does not exist by the given display name first.
    return Kingdom.findOne({ where: [{ name: identifer }] });
  }
}

import { Router } from 'express';

import infrastructureController from '../controllers/infrastructure.controller';
import { asyncErrorHandler } from './handlers/asyncError.handler';

// The infrastructure router will handle background requests for information that is related about
// the actual system. This will include a hello route (letting the user know that the server is up
// and running) and a version route (that will return the current server version and basic
// information).
const infrastructureRouter = Router();

infrastructureRouter.get('/hello', asyncErrorHandler(infrastructureController.hello));
infrastructureRouter.get('/version', asyncErrorHandler(infrastructureController.version));

export default infrastructureRouter;

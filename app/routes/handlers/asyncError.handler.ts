import { Request, Response, NextFunction } from 'express';
import httpCodes from '../../constants/httpCodes';

/**
 * A wrapper around the final request of a given router, if the router fails due to a unexpected
 * case or issue, then this will catch it. Responding to the user with a internal error message or a
 * stack trace + message in development or testing.
 */
export const asyncErrorHandler = (func: any) => async (request: Request, response: Response, next: NextFunction) => {
  try {
    return Promise.resolve(func(request, response, next));
  } catch (error) {
    // if we are in production and a internal server error occurs, just let the user know. We don't want to be exposing
    // any additional information that would help someone trying to gather internal information about the system. But during
    // development, ignore this and send back the message of the error and the stack that caused it.
    if (process.env.NODE_ENV === 'production') {
      return response
        .sendStatus(httpCodes.INTERNAL_SERVER_ERROR)
        .json({ message: 'Internal server error, something went wrong.' });
    }

    response.status(httpCodes.INTERNAL_SERVER_ERROR).json({ message: error.message, stack: error.stack });
  }
};

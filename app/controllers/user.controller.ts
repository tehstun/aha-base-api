import { Request, Response } from 'express';
import { getCustomRepository } from 'typeorm';
import * as _ from 'lodash';

import UserRepository from '../repository/user.repository';

import httpCodes from '../constants/httpCodes';
import { IUserRequest } from '../requests/IRequest';

export default class UserController {
  /**
   * Creates a new user based on the username, ensuring that the user does not already exist by the
   * username. Creating the new user and then finally returning said user.
   */
  public static async createNewUser(request: Request, response: Response) {
    if (_.isNil(request.body.username))
      return response.status(httpCodes.BAD_REQUEST).send({ message: 'invalid or empty username' });

    const userRepository = getCustomRepository(UserRepository);

    if (await userRepository.userExistsByUsername(request.body.username))
      return response
        .status(httpCodes.CONFLICT)
        .send({ message: 'user already exists by provided username' });

    const createdUser = await userRepository.createByUsername(request.body.username);
    return response.status(httpCodes.OK).json({ user: createdUser });
  }

  /**
   * Returns back the given username by the parameter. Since the user routes are pushed through the
   * user middleware (bindByParamUsername). The user object will exist on the request at the time
   * this is hit.
   */
  public static async getUserByUsername(request: IUserRequest, response: Response) {
    return response.json({ user: request.user });
  }
}

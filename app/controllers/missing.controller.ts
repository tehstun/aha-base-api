import { Request, Response } from 'express';

export default class MissingController {
  /**
   *
   * Handles routes that have been requested that dont exist.
   */
  public static handleMissing(request: Request, response: Response) {
    return response.status(404).send();
  }
}

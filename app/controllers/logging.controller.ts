import httpCodes from '../constants/httpCodes';
import logger from '../libs/logger';
import * as uuidv4 from 'uuid/v4';

export default class LoggingController {
  /**
   * Logs all request starts.
   */
  public static logRequestStart(req: any, res: any, next: () => void) {
    const sender = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    req.logInfo = { token: uuidv4().split('-')[0], time: Date.now() };

    logger.info(`${req.logInfo.token} Started ${req.method} ${req.originalUrl} for ${sender}`);
    res.on('finish', LoggingController.logRequestEnd.bind(this, req, res));
    return next();
  }

  /**
   * Logs all request ends.
   */
  public static logRequestEnd(req: any, res: any, next: () => void) {
    const { statusCode, _contentLength } = res;
    const { token, time } = req.logInfo;

    const message = `${token} Completed ${statusCode} ${_contentLength} in ${Date.now() - time}ms`;

    if (statusCode === httpCodes.OK || statusCode === httpCodes.NOT_MODIFIED) {
      logger.info(message);
    } else if (statusCode === httpCodes.INTERNAL_SERVER_ERROR) {
      logger.error(message);
    } else {
      logger.warn(message);
    }
  }
}

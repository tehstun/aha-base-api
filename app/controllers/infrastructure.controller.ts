import { Request, Response } from 'express';

const packageJson = require('../../package.json');
import httpCodes from '../constants/httpCodes';

export default class InfrastructureController {
  /**
   * The hello endpoint for validating the service is up.
   */
  public static hello(req: Request, res: Response) {
    return res.send('hello');
  }

  /**
   * Gets the current running version of the application to be sent back to the client. The version
   * will be pulled from the current package.json file for sending back, so make sure to keep the
   * package version up to date to allow valid version validation for a customer.
   */
  public static version(req: Request, res: Response) {
    const { name, version, author, license } = packageJson;

    return res.json({ name, version, author, license });
  }

  /**
   * Lets the user know that the endpoint being used is not valid/unavailable.
   */
  public static unavailable(req: Request, res: Response) {
    const description = 'The service is currently unavailable';
    return res.status(httpCodes.SERVICE_UNAVAILABLE).json({ error: 'Unavailable Service', description });
  }
}

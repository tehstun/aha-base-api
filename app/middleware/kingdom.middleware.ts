import { getCustomRepository } from 'typeorm';
import { Response, Request, NextFunction } from 'express';
import * as _ from 'lodash';

import httpCodes from '../constants/httpCodes';
import { IKingdomRequest } from '../requests/IRequest';
import KingdomRepository from '../repository/kingdom.repository';

export default class KingdomMiddleware {
  /**
   * A middleware designed to automatically bind a given kingdom that was specified in the url
   * paramter, this is used as a point of entry so that future requests do not have to go through
   * the process of ensuring that a given kingdom exists or not. If it made it to the request then
   * the kingdom exits.
   */
  public static async bindKingdomByParamIdentifier(
    request: IKingdomRequest,
    response: Response,
    next: NextFunction
  ) {
    const identifier = request.params.identifier;

    // ensure that we have a username to work from before attempting to continue. Otherwise we would
    // just be making a query based on nothing.
    if (_.isNil(identifier))
      return response
        .status(httpCodes.BAD_REQUEST)
        .json({ message: 'no kingdom identifer parameter was provided.' });

    const kingdomRepository = getCustomRepository(KingdomRepository);
    const kingdom = await kingdomRepository.findByIdentifier(identifier);

    // if the user does not exist, we cannot fullfil the complete request. SO lets go and let the user
    // know that the user does not exist and return out of the request.
    if (_.isNil(kingdom))
      return response
        .status(httpCodes.NOT_FOUND)
        .json({ message: `a kingdom does not exist by the provided identifer: ${identifier}.` });

    request.kingdom = kingdom;
    next();
  }
}

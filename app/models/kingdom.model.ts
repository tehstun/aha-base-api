import { Entity, Column } from 'typeorm';
import BaseModel from './base.model';

@Entity('kingdom')
export default class Kingdom extends BaseModel {
  /**
   * User facing name of the kingdom
   */
  @Column({ unique: true, type: 'text', name: 'kingdom_name' })
  public name: string;
}

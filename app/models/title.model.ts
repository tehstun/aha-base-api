import { Entity, Column } from 'typeorm';
import BaseModel from './base.model';

@Entity('title')
export default class Title extends BaseModel {
  /**
   * The name of the given title.
   */
  @Column({ unique: true, type: 'text', name: 'title_name' })
  public name: string;
}

import { Entity, Column } from 'typeorm';
import BaseModel from './base.model';

@Entity('item_category')
export default class ItemCategory extends BaseModel {
  /**
   * The name of the type of category.
   */
  @Column({ type: 'text', name: 'item_category_name' })
  public name: string;
}

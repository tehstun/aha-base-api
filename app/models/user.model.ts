import { Entity, Column, JoinTable, JoinColumn, ManyToOne, ManyToMany } from 'typeorm';
import BaseModel from './base.model';
import Kingdom from './kingdom.model';
import Item from './item.model';
import Outfit from './outfit.model';
import Title from './title.model';

@Entity('user')
export default class User extends BaseModel {
  /**
   * Twitch Username, preserve
   */
  @Column({ unique: true, type: 'text', name: 'user_username' })
  public username: string;

  /**
   * Twitch ID, Unique, Primary Key Field
   */
  @Column({ unique: true, type: 'text', name: 'user_twitch_id' })
  public twitchId: string;

  /**
   * In game currency
   */
  @Column({ type: 'int', name: 'user_gold', default: 0 })
  public gold: number;

  /**
   * In game point system
   */
  @Column({ type: 'int', name: 'user_fame', default: 0 })
  public fame: number;

  /**
   * The maximum amount of items that can be held by a player.
   */
  @Column({ type: 'int', name: 'user_inventory_limit', default: 15 })
  public inventoryLimit: number;

  /**
   * Set to 1 if the user wants to receive whispers upon entering the dungeon.
   */
  @Column({ type: 'boolean', name: 'user_dungeon_whisper', default: false })
  public dungeonWhisper: boolean;

  /**
   * RELATIONSHIPS
   */

  /**
   * Key of the kingdom in the TABLE_kingdoms
   */
  @ManyToOne((type) => Kingdom, (kingdom) => kingdom.id)
  @JoinColumn({ name: 'user_kingdom_id' })
  public kingdom: Kingdom;

  /**
   * The items in the users inventory.
   */
  @ManyToMany((type) => Item)
  @JoinTable()
  public inventory: Item[];

  /**
   * Get the users outfits
   */
  @JoinTable()
  @ManyToMany((type) => Outfit)
  public outfits: Item[];

  /**
   * Get the users titles
   */
  @JoinTable()
  @ManyToMany((type) => Title)
  public titles: Title[];
}

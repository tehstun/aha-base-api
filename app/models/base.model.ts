import { BaseEntity, CreateDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

export default class BaseModel extends BaseEntity {
  @PrimaryGeneratedColumn()
  public id: number;

  @UpdateDateColumn({ name: 'created_datetime' })
  public createdDatetime: Date;

  @CreateDateColumn({ name: 'updated_datetime' })
  public updatedDatetime: Date;
}

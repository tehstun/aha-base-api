# aha-base-api

Hi! Here is a structured core layout of the api we can use. Not all routes found
have been implemented but all the [Kingdom routes](https://gitlab.com/tehstun/aha-base-api/blob/master/app/routes/kingdom.router.ts) /
[controller](https://gitlab.com/tehstun/aha-base-api/blob/master/app/controllers/kingdom.controller.ts) and [creation/gathering user /
updating kingdom](https://gitlab.com/tehstun/aha-base-api/blob/master/app/routes/user.router.ts)
routes are done.

[Models are mapped out](https://gitlab.com/tehstun/aha-base-api/tree/master/app/models).

### Running

If you would like to create a running instance of the project you can do the following:

1. Make a copy of the .env.example and call it .env
2. open the .env and update the DB username and password to your postgres (mysql below)
3. run "npm install" on the project
4. run "npm run dev" to start the application.
5. go to "localhost:8080/api/v1.0/infrastructure/hello" and it should repond with "hello"
6. go to "http://localhost:8080/api/v1.0/kingdoms" and you should get a empty array of kingdoms.
7. yay!

#### MYSQL

To switch the connection to mysql do the following:

1. run "npm install mysql --save"
2. update your .env file DB_NAME, DB_USER, DB_PASS
3. remove the 5432 from the DB_PORT to equal "DB_PORT="
4. update the DIALECT to mysql
5. yay!

#### Postman

I have a [postman](https://www.getpostman.com/) enviroment and collection in which you can
use to create users, create kingdoms, set kingdoms and do all the gathering! here is the [collection
and here is the environment](https://gitlab.com/tehstun/aha-base-api/tree/master/postman_temp)
or ping me and I will share it directly with you via email :).

### Controllers

The controller design was used to split up each section into there own manageable controller of the
application, focusing on decoupling each section to reduce concerns about not being
separated. Each route having its own controller while adding some smaller controllers for
utility and logging reasons. Without this separation, the process of maintenance is far harder for
future developers.

### Services

The services layer is a high-level implementation of a service or services that can be used across
multiple controllers or other services.

### Repositories

Abstraction to raw SQL interfacing, all repository classes will focus around a
single table within the given database. Use of methods would be used directly instead of query
statements from each controller. Keeping maintenance high by allowing method implementations to
change in the future without having to trigger changes all around the application. While also being
extendable.

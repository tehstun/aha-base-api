import { env } from 'process';
import * as dotenv from 'dotenv';

dotenv.config();

import { networkIpAddress } from './app/libs/utilities';
import Server from './app/services/Server.service';
const projectConfiguration = require('./package');
import logger from './app/libs/logger';
import { config } from './config';

const port = Number(env.PORT) || config.PORT_APP || 8080;
const applicationServer = new Server();

/**
 * Called when a error occurring within the http server.
 * @param {Error} error The error that occurred.
 */
const handleServerError = (error: Error) => {
  logger.error(error);
};

/**
 * Called when the server starts properly listening, knowing we are fully operational.
 */
const handleListening = () => {
  logger.info(`initialized ${projectConfiguration.name}, version=v${projectConfiguration.version}`);
  logger.info(`http://localhost:${port}/ | http://${networkIpAddress}:${port}/ | process ${process.pid}`);
};

/**
 * Entry point, if parameters are used, then take up a parameters placement and shift the spread
 * operation right one, keeping the array still.
 * @param  {...any} params The parameters.
 */
const main = async () => {
  const server = await applicationServer.Start();

  server.listen(port);

  server.on('error', handleServerError);
  server.on('listening', handleListening);
};

main();
